>this config done manually, you can use plugin `vite-plugin-mpa`

# Django Vite Integration

## prepare django project

```sh
django-admin startproject django_vite
cd django_vite
```

then add template to test and configure static folder

## add vite project inside django 

```sh
pnpm create vite frontend --template react
pnpm i vite-plugin-compression -D
```

see `vite.config.js` for configs

>remove `index.html`, in vite project from now we will use django templates, see `templates/index.html`

>config in this example split entry points manually, we can use `vite-plugin-mpa` to split them autmatically

## integrate vite and django

1. add `vite_django` in `INSTALLED_APPS` before apps you want to use integration with them.
2. add `import 'vite/modulepreload-polyfill';` to beginning of every entrypoint is recommanded

see `settings.py` filee for configs