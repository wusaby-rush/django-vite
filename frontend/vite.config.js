import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import viteCompression from 'vite-plugin-compression';

// https://vitejs.dev/config/
export default defineConfig({
  // set base as Django's STATIC_URL
  base: '/static',
  build: {
    // generate manifest.json for Django's collectstatic
    manifest: true,
    // specify build directory, can be'static/' for Django's collectstatic
    outDir: 'dist/',
    // set entrypoints for components, can be single entrypoint for entire app
    // default is index.html and index.js in frontend if this option doesnt set
    rollupOptions: {
      input: {
        main: '/src/main.jsx',
        warning: '/src/warning.jsx',
        refrsh: 'refresh.js',
      }
    }
  },
  // add support for React, and zipping of build
  plugins: [react(), viteCompression()],
})
