import 'vite/modulepreload-polyfill';
import React from 'react'
import ReactDOM from 'react-dom/client'

function Warning() {
  const warningStyle = {
    color: 'white',
    padding: '1rem',
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'yellow',
  }

  return (
    <p style={warningStyle}>htis just a django-vite trial</p>
  )
}

ReactDOM.createRoot(document.getElementById('warning')).render(
  <React.StrictMode>
    <Warning />
  </React.StrictMode>,
)
